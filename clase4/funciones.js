
function leerNumero(titulo){
    let numero = prompt(titulo);

    if (numero == null || numero.length == 0){
        alert("Error. El valor no puede estar vacio");
        return 0;
    }else{
        let n = parseFloat(numero);
        if (isNaN(n)){
            alert("Numero invalido")
            return 0;
        }else
            return n;
    }    
}

function leerString(titulo,puedeEstarVacio){
    let cadena = prompt(titulo);

    if (puedeEstarVacio == true)
        return cadena;
    else
        if (cadena == null || cadena.length == 0){
            alert("Error. El valor no puede estar vacio");
            return false;
        }else
            return cadena;
}